MIRR="mirrport"
OVSBR="mega_bridge"
ovs-vsctl del-port $OVSBR $MIRR
ovs-vsctl add-port $OVSBR $MIRR -- \
    set interface $MIRR type=internal

#-- --id=@m create mirror name=m0 select-all=true output-port=@p \
ovs-vsctl --id=@p get port $MIRR \
    -- --id=@m create mirror name=m0 select-all=true select-vlan=101 output-port=@p \
    -- set bridge mega_bridge mirrors=@m
ip link set up dev $MIRR
