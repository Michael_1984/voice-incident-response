# Voice Incident Response Platform

## Introduction  

The voice incident response platform (Virp) is a testing environment to showcase response to an attack using only voice commands. This bundle of scripts is meant to be ran on a single computer and will launch a network of 10 virtual machines. The virtual machines run tiny core linux. Of the 10 virtual machines one will randomly become an attacker while another becomes a victim and the rest simply download web traffic from each other. There is a backend web server that captures all the packets and sends them via its api to a chrome web browser where the user can view the network and use voice commands to change the view or kill virtual machines. The idea being that this software bundle showcases how easy it 'can' be to see and stop an attack. 

## Requirements  

To use this you must have a linux computer with: qemu, open-vswitch, minimega, chrome web browser. All of these are free applications that can be downloaded online or installed using apt-get. A tinycore virtual machine is also required that starts up and automatically runs /share/runme.sh??? You can ask me for the tinycore qcow image. 

## How to use

Using the platform is very easy once all the requirements are installed. Simply run start_all.sh and wait for it to finish then launch your chrome browser and navigate to https://localhost 

## Components

### Virtual Machines  
The virtual machines that are launched are all tinycore linux. The virtual machine was designed to automatically startup and run /share/runme.sh as long as the tinycore .qc2 image used does that and has python/scapi this project will work. 

### Virtual Environment  

The virtual environment is all possible due to minimega which can be downloaded for free from minimega.org. Minimega uses openvswitch to do all of its vlan and virtual network magic so that is also required. This can only run on linux. 

Process  
* launch virtual machines (See startvms.sh)  
* detect ip addresses of virtual machines (See getips.sh)
* pick a badguy and a victim make badguy attack victim (See play_game.sh)
* launch webserver 

### Webserver  

The webserver accepts a parameter 'capdev' which should be the mirror/span device to sniff all the traffic from. This is automatically handled in startall.sh. The webserver provides two api calls '/speech' and '/map'. The '/map' api call produces a current network map of devices so that they can be displayed in a web browser. The '/speech' api call is a input and takes a command to forward back to the underlying system. See the source code main.go in the /webserver directory for full implementation of the webserver, but in a nutshell it provides full packet decoding and keeps track of the packets per second of each ethernet interface as well as host files to load in the be browser. 

### Webpage  

The webpage serves to show a diagram of the network and accept voice commands. Diagramming is done using vis.js package, and the voice commands are handled simply using artyom which uses google speech recognition. Since the webpage uses google speech recognition it needs to use google chrome browser and will only work with google chrome browser. Additional commands are easy to add in the addCommands function, wildcards * are for 'any' text and basically serve as parameters for the command. The command 'show *' when spoken will rebuild the network diagram and show only nodes that the spoken node connects to. The command 'kill *' takes the spoken command and calls the backend '/speech' api call in the webserver, this command is sent to the webserver but the webserver does not do anything with the data, this is left for the reader to implement. 

## Developer Guidelines  

This project was meant as a jumping off board to develop more complex scenarios. It is highly recommended that you fork this code and use it in your own projects, use whatever parts fit your needs and ditch the rest.  
Deployment of scripts is handled in playgame.sh if you want to develop differnt attacks start with adding a script to launch the attack in the /runnables folder and have playgame.sh push it (instead of dos.py). The runnable script can be a python attack or a bash script. 
After another attack is added some way to represent it must be implemented it in the webserver, more decoding of packets to show specific types of attacks such as different ports would be added.  
After that the visualization in the webpage should be modified to somehow show the attack as different from the rest of the 'normal' traffic. Esentially another color, dotted line or something of that sort should be added if a different attack type is suspected. From there voice commands can be used to futher investigate the diagram and eventually kill the offending node. 

## Further Work  
This project was by no means finished. Further work on this project would include an automated scoring system where after you kill a node with voice commands the web page tells you if you were correct, because esentially thats all this project did, killing the node or blocking the attack is just an extra nicity. Once you have identified the 'bad guy' whatever happens beyond that isnt really important to pass the 'test'. Implementing differnet views that show tcp ports, udp ports, as well as the current existing ethernet diagram. 

## Troubleshooting  

Webpage does not load -> be sure to use http(s) it must use https  

Voice commands dont work -> you must use the actual chrome broswer, not chromium or anything else.
