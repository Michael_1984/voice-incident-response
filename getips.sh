#!/bin/bash

# this just does some magic to grab the ips of the vms and store in a text file
# if they pass in an arg skip the thing and just use the exising files
if [ -z "$1" ]; then
    rm mgmt_tap_arp.txt vmips.txt ips.txt badguyip.txt
	# kick off tcpdump
	mgmttap=$(ip a | grep "10.0.1.1" | cut -d' ' -f9)
	echo "mgmt tap on: $mgmttap"
	tcpdump arp -i $mgmttap > mgmt_tap_arp.txt&
	sleep 5
	onlineip=`grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" mgmt_tap_arp.txt | sort | uniq | wc -l`
	while [ "$onlineip" -eq "0" ]; do 
		echo "waiting, found: $onlineip"
		onlineip=`grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" mgmt_tap_arp.txt | sort | uniq | wc -l`
		sleep 3
	done
    sleep 3
	# stop tcpdump
	killall tcpdump
	echo "ready"
	onlineip=`grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" mgmt_tap_arp.txt | sort | uniq`

	echo $onlineip > vmips.txt
	chown michael:michael vmips.txt
fi

rm ips.txt
chown michael:michael vmips.txt
for ip in $(cat vmips.txt); do 
    echo "$ip"
	echo "$ip">>ips.txt
done
# randomize the ips again
sort -R ips.txt > randips.txt
# pick out the bad guy
head -1 randips.txt > badguyip.txt
# take bad guy out of other ips
cat randips.txt | grep -E -v "$(cat badguyip.txt)" > tmp
mv tmp randips.txt
