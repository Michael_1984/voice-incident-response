#!/bin/bash
# get the ip's on the play vlan side 10.0.0.x
rm tmp.txt tmp2.txt
# note we have already removed badguy ip from randips
for ip in $(cat randips.txt); do ssh -o "StrictHostKeyChecking=no" tc@$ip 'ifconfig | grep "10.0.0"'>> tmp.txt; done
grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" tmp.txt > tmp2.txt
cat tmp2.txt | grep -E -v '255' > playips.txt
head -1 playips.txt > victimip.txt

# move list of ips and basic script to all vms
for ip in $(cat randips.txt); do 
    scp ./playips.txt tc@$ip:/share/ips.txt
    scp ./runnables/get_webpage.sh tc@$ip:/share/runme.sh
done

# todo make badguy do something else
for ip in $(cat badguyip.txt); do 
    # do this just so it does not prompt when pushing files
    scp -o StrictHostKeyChecking=no ./runnables/dos.py tc@$ip:/share/dos.py
    scp -o StrictHostKeyChecking=no ./victimip.txt tc@$ip:/share/
    #  calls the dos and injects the IP
    scp -o StrictHostKeyChecking=no ./runnables/rundos.sh tc@$ip:/share/runme.sh
    
done
