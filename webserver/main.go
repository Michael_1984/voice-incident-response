package main

import (
    "fmt"
    "flag"
    "math"
    "time"
    "encoding/json"
    "log"
    "net"
    "net/http"
    "sync"
    "github.com/google/gopacket"
    "github.com/google/gopacket/layers"
    "github.com/google/gopacket/pcap"
)

// Data Structure
// map['key'] holds stats where key can be 
// eth!ipsrc!ipdst!proto!src!dst
// not all protos will have src and dst, they will be 0 when nonexistant

// this is the meta keeps track of stats
type FlowStat struct {
    FirstSeen time.Time //last packet read time stamp
    LastSeen time.Time //last packet read time stamp
    PktSec				float64 // packets per second
    PktCnt				float64 // how many packets have we seen
    Old                 bool
}

type SyncMap struct{
    sync.RWMutex
    m map[string]FlowStat
}


var (
    capdev = flag.String("capdev", "eth0", "capture device")
    snapshot_len int32  = 1024
    promiscuous  bool   = true
    err          error
    timeout      time.Duration = 30 * time.Second
    handle       *pcap.Handle
    dMap         = SyncMap{m: make(map[string]FlowStat)}
    pMap         = make(map[string]FlowStat)
)

type PacketInfo struct {
	Id                 uint64
    Proto              uint8
	SrcIp, DstIp       net.IP
	SrcMac, DstMac     string
	SrcPort, DstPort   uint16
	SrcColor, DstColor uint32
	Data               []byte //this is session layer and above
}

func init() {
	flag.Parse()
	if *capdev== "" {
		log.Fatal("bad capdev")
	}
}



func handlePacket(pkt gopacket.Packet) (PacketInfo, error) {
	var pktInfo PacketInfo
	var eth layers.Ethernet
	var dot1q layers.Dot1Q
	var ip4 layers.IPv4
	var icmp layers.ICMPv4
	var ip6 layers.IPv6
	var tcp layers.TCP
	var udp layers.UDP
	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
        &dot1q,
		&eth,
		&ip4,
		&icmp,
		&ip6,
		&tcp,
		&udp)
	decoded := []gopacket.LayerType{}
    // this normally returns at the 5th layer payload b/c no decoder
    // but by then we have all the data we want (for now)
	if err := parser.DecodeLayers(pkt.Data(), &decoded); err != nil {
		//fmt.Printf("Decode layers failed: %v pkt %v\n", err, pkt)
        // if we return we will never write to the map ! :(
		//return pktInfo, err
	}
    key := "base"
    // this goes over every layer so we can travers layers in our study
    // NOTE: for demo we are only doing L2, but we can do all the way to L7 if we uncomment out 
    // NOTE: actually for demo we are just doing ip, so this is an ipv4 view, everything based off ip only
	for _, layerType := range decoded {
		//fmt.Printf("layer type %v\n",layerType)
		switch layerType {
		case layers.LayerTypeEthernet:
			pktInfo.SrcMac = eth.SrcMAC.String()
			pktInfo.DstMac = eth.DstMAC.String()
            //key = key+eth.SrcMAC.String()+"!"+eth.DstMAC.String()
		    //fmt.Printf("eth SRC: %v DST: %v\n", pktInfo.SrcMac, pktInfo.DstMac)
        case layers.LayerTypeDot1Q:
            // dont care about the vlan id at this point, normally 101
		    //fmt.Printf("vlanid: %v\n",dot1q.VLANIdentifier)
		case layers.LayerTypeICMPv4:
            // 1 should be set by ip4.Protocol in ipv4 layer
            // pktInfo.Proto = uint8(1)
		    //fmt.Printf("icmp SRC: %v DST: %v\n", pktInfo.SrcMac, pktInfo.DstMac)
		case layers.LayerTypeIPv4:
			pktInfo.SrcIp = ip4.SrcIP
			pktInfo.DstIp = ip4.DstIP
            // ip packets can have proto tcp(6), udp(17), ICMPv4(1) and so on, see
            // https://github.com/google/gopacket/blob/master/layers/enums.go
			pktInfo.Proto = uint8(ip4.Protocol)
            key = key+"!"+ip4.Protocol.String()+"!"+ip4.SrcIP.String()+"!"+ip4.DstIP.String()
		    //fmt.Printf("ipv4 SRC: %v DST: %v\n", pktInfo.SrcIp, pktInfo.DstIp)
		case layers.LayerTypeIPv6:
            // we should not support v6 yet
			//pktInfo.SrcIp = ip6.SrcIP
			//pktInfo.DstIp = ip6.DstIP
			//pktInfo.Proto = uint8(ip6.NextHeader)
		    //fmt.Printf("unsupported ipv6 SRC: %v DST: %v\n", pktInfo.SrcIp, pktInfo.DstIp)
		case layers.LayerTypeUDP:
			pktInfo.SrcPort = uint16(udp.SrcPort)
			pktInfo.DstPort = uint16(udp.DstPort)
            //key = key+"!"+udp.SrcPort.String()+"!"+udp.DstPort.String()
		    //fmt.Printf("udp SRC: %v DST: %v SrcPort: %v DstPort: %v\n", pktInfo.SrcIp, pktInfo.DstIp, pktInfo.SrcPort, pktInfo.DstPort)
		case layers.LayerTypeTCP:
			pktInfo.SrcPort = uint16(tcp.SrcPort)
			pktInfo.DstPort = uint16(tcp.DstPort)
            //if !tcp.SYN { // we only care about 
            //    return pktInfo, nil //
            //}
            // test to see if we can just get l2 ?
            //key = key+"!"+tcp.SrcPort.String()+"!"+tcp.DstPort.String()
		    //fmt.Printf("tcp SRC: %v DST: %v SrcPort: %v DstPort: %v\n", pktInfo.SrcIp, pktInfo.DstIp, pktInfo.SrcPort, pktInfo.DstPort)
        }
	}


    //fmt.Printf("key: %v time: %v\n", key, time.Now())
    dMap.Lock()
    old, ok := dMap.m[key]
    if !ok {
        old = FlowStat{time.Now(),time.Now(),0,0,false}
    }
    old.PktCnt= old.PktCnt + 1
    old.LastSeen = time.Now()
    dMap.m[key]=old
    dMap.Unlock()
    // TODO remove all old connections, basically the return port will just be old.
	return pktInfo, nil
}

// this prints out the current map 
func printMap() {
    for {
        time.Sleep(2*time.Second)
        //fmt.Printf("Map at %v\n",time.Now())
        dMap.Lock()
        // wipe and make new printable map
        // pMap = make(map[string]FlowStat)
        // wipe any old stuff
        for k := range pMap{
            val, _ := pMap[k]
            now := time.Now()
            elapsed := float64(now.Sub(val.LastSeen)/1000000000)
            if elapsed > 150{
                val.Old = true
            }
        }
        for k := range dMap.m {
            val, _ := dMap.m[k]
            now := time.Now()
            //
            //elapsed := now.Sub(val.LastSeen)
            //fmt.Print("Elapsed %v\n",elapsed)
            //if elapsed < (30*time.Second){
            //    pMap[k] = val
            //}
            elapsed := float64(now.Sub(val.LastSeen)/1000000000)
            val.PktSec = ((val.PktCnt/elapsed))
            // filter out really small and large values, dont want to deal with them
            if val.PktSec < 1 || val.PktSec > 100000000{
                val.PktSec = 0
            }
            val.PktSec = math.Round(val.PktSec*100)/100
            //fmt.Printf("key: %v elapsed: %v pps: %v\n", k, elapsed, val.PktSec)
            /*
            v2, ok := pMap[k]
            if ok{ // if we had an old one we can calc pps
                var pps int32
                pps = ((val.PktCnt - v2.PktCnt)/2)
                val.PktSec = pps
                fmt.Printf("new: %v old: %v diff: %v\n", val.PktCnt, v2.PktCnt,pps)
                val.PktCnt = 0
            }
            cpy := FlowStat{time.Now(),0,0}
            cpy.PktCnt = val.PktCnt
            cpy.PktSec = val.PktSec
            pMap[k] = cpy
            */
            // we want to just see stuff that we've seen in the last 2 minutes
            if elapsed < 150 && k != "base"{
                val.Old = false
                pMap[k] = val
                //fmt.Printf("key: %v elapsed: %v pps: %v\n", k, elapsed, val.PktSec)
            }
        }
        /*
        for k := range pMap {
            val, ok := pMap[k]
            if ok {
                fmt.Printf("key: %v count: %v\n", k, val.PktCnt)
           }
        }
        */
        dMap.Unlock()
    }
}

func webServer(){

    //http.Handle("/", http.FileServer(http.Dir("./static")))
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        http.ServeFile(w, r, r.URL.Path[1:])
    })

    http.HandleFunc("/speech", func(w http.ResponseWriter, r *http.Request) {
        fmt.Printf("speech request %v\n", r)
        fmt.Fprintf(w, "ok")
    })
    http.HandleFunc("/map", func(w http.ResponseWriter, r *http.Request) {
        jsonString, err := json.Marshal(pMap)
        fmt.Println(err)
        fmt.Fprintf(w, string(jsonString))
    })

    log.Fatal(http.ListenAndServeTLS(":443","server.crt","server.key", nil))

}


func main() {
    // Open device
    handle, err = pcap.OpenLive(*capdev, snapshot_len, promiscuous, timeout)
    if err != nil {
        log.Fatal(err)
    }
    defer handle.Close()

    go printMap()// print map to console
    go webServer()// print map to console
    // Use the handle as a packet source to process all packets
    packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
    for packet := range packetSource.Packets() {
        // Process packet here
        //fmt.Println(packet)
        _,err = handlePacket(packet)
        if err != nil {
            //log.Fatal(err)
        }
    }
}
