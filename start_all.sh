#!/bin/bash
echo "starting $SECONDS"
echo "stopping anything that could be started"
./stopmm.sh
rm -rf /tmp/minimega
./rm_mega_taps.sh
killall webserver
sleep 5
echo "starting minimega and miniweb"
./startmm.sh
sleep 5
echo "starting the vms"
./startvms.sh
echo "finding ips"
./getips.sh
echo "done in $SECONDS"
echo "starting game"
./play_game.sh

echo "starting mirror port and webserver"
# game has started make mirror
./start_mirror.sh
./webserver/webserver --capdev mirrport

echo "ready go to http://127.0.0.1:8081"
