mm='/usr/bin/minimega -e'


$mm clear vm config 
$mm vm config disk /home/michael/ui/finalproj/tcsvr.qc2
$mm vm config memory 256
$mm vm config net net1 mgmt
$mm tap create net1 ip 10.0.0.1/24
$mm dnsmasq start 10.0.0.1 10.0.0.2 10.0.0.254
$mm tap create mgmt ip 10.0.1.1/24
$mm dnsmasq start 10.0.1.1 10.0.1.2 10.0.1.254
$mm vm launch kvm 10
$mm vm start all

sleep 5
